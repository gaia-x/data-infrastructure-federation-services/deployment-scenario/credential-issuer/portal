#!/bin/sh

sed -i "s|__REACT_APP_KEYCLOAK_URL__|${REACT_APP_KEYCLOAK_URL}|g" /usr/share/nginx/html/static/js/*.js
sed -i "s|__REACT_APP_ICP_URL__|${REACT_APP_ICP_URL}|g" /usr/share/nginx/html/static/js/*.js

nginx -g 'daemon off;'