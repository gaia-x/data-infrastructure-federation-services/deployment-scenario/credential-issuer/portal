import FormSchema from '@rjsf/mui';
import Box from '@mui/material/Box';
import icpApi from 'services/icp-api';
import { toast } from 'react-toastify';
import React, { useEffect } from 'react';
import validator from '@rjsf/validator-ajv8';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';

const Form = ({ vcName, schema, handleChildResponse }) => {

    const onSubmit = async ({ formData }) => {
        try {
            const response = await icpApi.continueIssuance(vcName, { type: 'form', form: formData });
            if (response.data !== '') {
                handleChildResponse(response.data);
            } else {
                toast.error('There was a problem with your request. Please try again.');
            }
        } catch (error) {
            toast.error(`An error occurred: ${error.message}`);
        }
    };

    useEffect(() => {
        // async function LoadPlugins() {
        //     if (schema && schema.pluginControl) {
        //         let variables = schema.variables;
        //         for (const key in variables) {
        //             if (variables.hasOwnProperty(key)) {
        //               window[key] = variables[key];
        //             }
        //         }
        //         const script = document.createElement('script');
        //         script.src = schema.pluginControl;
        //         script.async = true;
        //         document.body.appendChild(script);
        //         return () => {
        //             document.body.removeChild(script);
        //         }
        //     }
        // }

        //LoadPlugins();
    }, [schema]);

    return (
        <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            minHeight: '90vh',
            flexDirection: 'column',
        }}>
            {schema ? (
                <>
                    <Typography variant="h5" component="h3" sx={{ textAlign: 'center', mb: 4, mt: 2 }}>
                        Please fill this form to generate your Verifiable Credential
                    </Typography>

                    <Box sx={{
                        width: '100%',
                        maxWidth: '800px',
                        margin: '0 auto',
                        padding: '0 20px',
                    }}>
                        <FormSchema schema={schema} validator={validator} onSubmit={onSubmit} />

                        <style>
                            {`
                                form button[type="submit"] {
                                    position: relative;
                                    left: 50%;
                                    transform: translateX(-50%);
                                    margin-bottom: 30px;
                                }
                            `}
                        </style>
                    </Box>
                </>
            ) : (
                <CircularProgress />
            )}
        </Box>
    );
};

export default Form;