
import Box from '@mui/material/Box';
import icpApi from 'services/icp-api';
import QrCodeComponent from "./QrCode";
import { toast } from 'react-toastify';
import React, { useEffect, useRef } from 'react';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';


const Presentation = ({ openidUriPresentation, state, handleChildResponse }) => {

    const currentVerifier = useRef();

    useEffect(() => {
        let requestInProgress = false;

        const cleanUp = () => {
            if (currentVerifier.current) {
                clearInterval(currentVerifier.current);
            }
        };

        async function startVerifier() {
            currentVerifier.current = setInterval(() => {
                if (!requestInProgress) {
                    requestInProgress = true;
                    icpApi.isVerified(state)
                        .then(response => {
                            if (response.status === 200 && response.data.message !== "Pending" && (response.data.url !== "" || response.data.format !== "")) {
                                handleChildResponse(response.data);
                                cleanUp();
                            }
                            else if (response.status === 200 && response.data.message === "Pending") {
                                console.log("pending");
                            }
                            else {
                                toast.error(response.response.data.error || "Unknown error");
                                cleanUp();
                            }
                        })
                        .catch(error => {
                            toast.error(`Error : ${error.message}`);
                            cleanUp();
                        })
                        .finally(() => {
                            requestInProgress = false;
                        });
                }
            }, 2000);
        }

        if (openidUriPresentation !== '' && state !== '') startVerifier();

        return cleanUp;
    }, [openidUriPresentation, state, handleChildResponse]);

    return (
        <Box sx={{
            padding: 2,
            display: 'flex',
            textAlign: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            justifyContent: 'center'
        }}>
            {openidUriPresentation !== '' && state !== '' ? (
                <>
                    <Typography variant="h5" component="h3" sx={{ marginBottom: 2 }}>
                        Please scan the QR Code to present your credentials
                    </Typography>
                    <QrCodeComponent url={openidUriPresentation} />
                </>
            ) : (
                <CircularProgress />
            )}
        </Box>
    );
};

export default Presentation;