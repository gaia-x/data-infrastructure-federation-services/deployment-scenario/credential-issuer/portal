import PropTypes from 'prop-types'
import QRCode from 'react-qr-code';
import React, { useState } from 'react';
import { Alert, Button, ButtonBase, Box, Typography, useMediaQuery } from '@mui/material';

const QrCode = ({ url }) => {
    const [showAlertCopyUri, setShowAlertCopyUri] = useState(false);

    const isMobile = useMediaQuery('(max-width:500px)');

    const handleCopyClick = () => {
        navigator.clipboard.writeText(url)
            .then(() => {
                showAlertWithTimeout();
            }).catch((error) => {
                console.error('Failed to copy to clipboard:', error);
            });
    };

    const showAlertWithTimeout = () => {
        setShowAlertCopyUri(true);
        setTimeout(() => {
            setShowAlertCopyUri(false);
        }, 2000);
    };

    return (
        <Box sx={{ textAlign: 'center', padding: 2 }}>
            {isMobile ? (
                <ButtonBase component="a" href={url} sx={{ flexDirection: 'column', alignItems: 'center', textAlign: 'center' }} target="_blank">
                <img src="/DeeplinkLogo.jpg" alt="Get credentials" style={{ width: 100, height: 100 }} />
                <Typography variant="body1" sx={{ mt: 1 }}>
                    Deeplink
                </Typography>
            </ButtonBase>
            ) : (
                <>
                    <QRCode
                        value={url}
                        size={300}
                        bgColor="#FFFFFF"
                        fgColor="#000000"
                        level="L"
                    />

                    <Typography variant="body1" sx={{ fontStyle: 'italic', fontSize: '14px', color: 'rgb(139, 140, 140, 1)', mt: 2 }}>
                        Having trouble scanning QR code?<br />Just copy the URL
                    </Typography>

                    <Box component="pre" sx={{ width: '300px', overflow: 'auto', my: 2, bgcolor: 'background.paper', p: 1 }}>
                        <code id="openid-uri">
                            {url}
                        </code>
                    </Box>

                    <Button variant="contained" onClick={handleCopyClick} sx={{ mb: 2 }}>
                        COPY URL
                    </Button>

                    {showAlertCopyUri && (<Alert severity="success" sx={{ position: 'fixed', bottom: 20, left: '50%', transform: 'translateX(-50%)', width: 'auto', maxWidth: '90%' }}>URL copied to clipboard</Alert>)}
                </>
            )}
        </Box>
    );
};

QrCode.propTypes = {
    url: PropTypes.string.isRequired
};

export default QrCode;