import Form from 'components/Form';
import Box from '@mui/material/Box';
import icpApi from 'services/icp-api';
import { toast } from 'react-toastify';
import keycloakApi from 'services/keycloak-api';
import QrCodeComponent from 'components/QrCode';
import Typography from '@mui/material/Typography';
import React, { useState, useEffect } from 'react';
import Presentation from 'components/Presentation';
import { useParams, useLocation } from 'react-router-dom';
import CircularProgress from '@mui/material/CircularProgress';


const Issuance = () => {
    const [formSchema, setFormSchema] = useState();
    const [openidUriIssuance, setOpenidUriIssuance] = useState('');
    const [openidUriPresentation, setOpenidUriPresentation] = useState('');
    const [openidPresentationState, setOpenidPresentationState] = useState('');
    const { vcName } = useParams();

    const useQuery = () => {
        return new URLSearchParams(useLocation().search);
    };
    const query = useQuery();
    const typeOfIssuance = query.get('typeOfIssuance');
    const keycloakCode = query.get('code');


    const handleChildResponse = (response) => {
        if (response.openidUri && response.openidUri.length !== 0) {
            setOpenidUriIssuance(response.openidUri);
        }
        else {
            toast.error("There was a problem with your request. Please try again.");
        }
    };

    useEffect(() => {
        /**
         * Get the keycloak URL for login if typeOfIssuance === 'keycloak' (startIssuance)
         */
        async function getKeycloakUrl() {
            try {
                const response = await icpApi.startIssuance(vcName, typeOfIssuance);
                if (response.status === 200 && response.data !== '') {
                    window.location.href = `${response.data.redirectUri}&redirect_uri=${window.location.origin}/issuer/${encodeURIComponent(vcName)}`;
                }
                else {
                    toast.error("There was a problem with your request. Please try again.");
                }
            } catch (error) {
                toast.error(`An error occurred: ${error.response}`);
            }
        }

        /**
         * Get the openid URI issuance by the keycloak code (continueIssuance)
         */
        async function getOpenidUriIssuanceByKeycloak() {
            try {
                const token = await keycloakApi.getToken(vcName, keycloakCode);
                if (token === undefined || token === '') throw new Error('Error to get keycloak token');
                const jwtVcFormat = {
                    "format": "jwt_vc_json",
                    "types": [
                        "VerifiableCredential",
                        vcName
                    ],
                    "proof": {
                        "proof_type": "jwt",
                        "jwt": token
                    }
                };
                const response = await icpApi.continueIssuance(vcName, { type: 'keycloakJWT', keycloakJWT: jwtVcFormat });
                if (response.status === 200 && response.data !== '') {
                    handleChildResponse(response.data);
                }
            }
            catch (error) {
                toast.error(`An error occurred: ${error.message}`);
            }
        }

        async function fetchData() {
            try {
                const response = await icpApi.startIssuance(vcName, typeOfIssuance);
                if (typeOfIssuance === 'form') {
                    setFormSchema(response.data.form);
                }
                else if (typeOfIssuance === 'presentation') {
                    setOpenidUriPresentation(response.data.openidUri);
                    setOpenidPresentationState(response.data.state);
                }
                else if (typeOfIssuance === 'keycloak') {
                    getKeycloakUrl();
                }
            }
            catch (error) {
                toast.error(error.message);
            }
        }

        if (keycloakCode != null)
            getOpenidUriIssuanceByKeycloak();
        else
            fetchData();
    }, [vcName, typeOfIssuance, keycloakCode]);

    return (
        (() => {
            if (vcName && openidUriIssuance !== '') {
                return (
                    <Box sx={{
                        padding: 2,
                        display: 'flex',
                        textAlign: 'center',
                        alignItems: 'center',
                        flexDirection: 'column',
                        justifyContent: 'center'
                    }}>
                        <Typography variant="h5" component="h3" sx={{ marginBottom: 2 }}>
                            Please scan the QR Code to receive your credential
                        </Typography>

                        <QrCodeComponent url={openidUriIssuance} />
                    </Box>
                )
            }
            else if (vcName && typeOfIssuance === 'form') {
                return (
                    <Form vcName={vcName} schema={formSchema} handleChildResponse={handleChildResponse} />
                )
            }
            else if (vcName && typeOfIssuance === 'presentation') {
                return (
                    <Presentation openidUriPresentation={openidUriPresentation} state={openidPresentationState} handleChildResponse={handleChildResponse} />
                )
            }
            else {
                return (
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: 2,
                        minHeight: '100vh',
                        textAlign: 'center',
                    }}>
                        <CircularProgress />
                    </Box>
                )
            }
        })()
    );
};

export default Issuance;