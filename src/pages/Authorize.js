
import Box from '@mui/material/Box';
import icpApi from 'services/icp-api';
import { toast } from 'react-toastify';
import React, { useState } from 'react';
import Button from '@mui/material/Button';
import { useLocation } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';


const Authorize = () => {
    const [username, setUsername] = useState('');

    const useQuery = () => {
        return new URLSearchParams(useLocation().search);
    };
    const query = useQuery();
    const requestUri = query.get('request_uri');
    const issuerState = query.get('issuer_state');

    const handleChange = (event) => {
        setUsername(event.target.value);
    };

    const handleSubmit = async () => {
        try {
            const state = requestUri ? { state: requestUri } : { state: issuerState };
            const response = await icpApi.authorize(state, username);
            if (response.status === 200 && response.data.redirect_url !== '') {
                window.location.href = response.data.redirect_url;
            } 
            else if (response.response.status === 401) {
                toast.error(response.response.data.error);
            }
            else {
                toast.error('There was a problem with your request. Please try again.');
            }
        } catch (error) {
            toast.error(`An error occurred: ${error.message}`);
        }
    };

    return (
        <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            minHeight: '93vh',
            background: 'linear-gradient(270deg, #000f8e 25%, #a931f6 129.76%)',
            padding: '20px',
        }}>
            <Box sx={{ maxWidth: 400, width: '100%', backgroundColor: '#fff', borderRadius: '5px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)', margin: '20px', padding: '20px' }}>
                <Typography variant="h4" component="h4" sx={{ textAlign: 'center' }}>Login</Typography>
                <Typography variant="body1" sx={{ fontStyle: 'italic', fontSize: '14px', color: 'rgb(139, 140, 140, 1)', mt: 2 }}>
                    Use 1234
                </Typography>
                <TextField label="Username" variant="outlined" onChange={handleChange} fullWidth sx={{ marginTop: '20px' }} />
                <Button variant="contained" onClick={handleSubmit} fullWidth sx={{ marginTop: '20px' }}>
                    Next
                </Button>
            </Box>
        </Box>
    );
};

export default Authorize;