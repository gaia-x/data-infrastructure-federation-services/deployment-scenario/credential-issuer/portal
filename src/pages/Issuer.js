import { router } from "router";
import Box from '@mui/material/Box';
import icpApi from 'services/icp-api';
import Step from '@mui/material/Step';
import { toast } from 'react-toastify';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import Stepper from '@mui/material/Stepper';
import { getOidcVersions } from 'utils/oidc';
import MenuItem from '@mui/material/MenuItem';
import { useLocation } from 'react-router-dom';
import QrCodeComponent from 'components/QrCode';
import StepLabel from '@mui/material/StepLabel';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import React, { useState, useEffect } from 'react';
import StepContent from '@mui/material/StepContent';
import FormControl from '@mui/material/FormControl';
import CircularProgress from '@mui/material/CircularProgress';

const Issuer = () => {
  const [vcList, setVcList] = useState([]);
  const [selectedVC, setSelectedVC] = useState('');
  const [activeStep, setActiveStep] = useState(0);

  const useQuery = () => {
    return new URLSearchParams(useLocation().search);
  };
  const query = useQuery();
  const openidUri = query.get('openidUri');
  const vcNameFromURL = query.get('credential');

  const handleNext = () => {
    if (activeStep === 0 && selectedVC) {
      router.navigate(`?credential=${encodeURIComponent(selectedVC)}`);
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    if (activeStep === 1) {
      router.navigate(window.location.pathname);
    }
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleChangeVC = (event) => {
    setSelectedVC(event.target.value);
  };

  const handleStartIssuance = (type) => {
    try {
      getOidcVersions();
      router.navigate(`/issuer/${encodeURIComponent(selectedVC)}?typeOfIssuance=${encodeURIComponent(type)}`);
    } catch (error) {
      toast.error(error.message);
    }
  }

  const steps = [
    {
      label: 'Select credentials to issue'
    },
    {
      label: 'Choose the type of issuance'
    },
  ];

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await icpApi.getVcToIssue();
        if (response.data && response.data !== '') {
          setVcList(response.data);
          if (vcNameFromURL) {
            const vcFromList = response.data.find(item => item.name === vcNameFromURL);
            if (vcFromList) {
              setSelectedVC(vcNameFromURL);
              setActiveStep(1);
            }
          }
        }
        else {
          toast.error('Error to get credentials, please try again.');
        }
      }
      catch (error) {
        toast.error(error.message);
      }
    }
    fetchData();
  }, [vcNameFromURL]);

  return (
    openidUri ? (
      <Box sx={{
        padding: 2,
        display: 'flex',
        textAlign: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center'
      }}>
        <Typography variant="h5" component="h3" sx={{ marginBottom: 2 }}>
          Please scan the QR Code to receive your credential
        </Typography>
        {/* Assurez-vous que openidUriIssuance est correctement défini basé sur openidUri */}
        <QrCodeComponent url={decodeURIComponent(openidUri)} />
      </Box>
    ) : vcList && vcList.length > 0 ? (
      <Box sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '93vh',
        background: 'linear-gradient(270deg,#000f8e 25%,#a931f6 129.76%)',
        padding: '20px',
      }}>
        <Box sx={{ maxWidth: 400, width: '100%', backgroundColor: '#fff', borderRadius: '5px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)', margin: '20px', padding: '20px' }}>
          <Stepper activeStep={activeStep} orientation="vertical">
            {steps.map((step, index) => (
              <Step key={step.label}>
                <StepLabel
                  optional={index === 2 ? <Typography variant="caption">Last step</Typography> : null}
                >
                  {step.label}
                </StepLabel>
                <StepContent>
                  {(() => {
                    if (index === 0) {
                      return (
                        <FormControl fullWidth>
                          <InputLabel id="vc-select-label">Credentials</InputLabel>
                          <Select
                            labelId="vc-select-label"
                            id="vc-select"
                            value={selectedVC}
                            onChange={handleChangeVC}
                            label="Credentials"
                          >
                            {vcList.map((vc) => (
                              <MenuItem key={vc.name} value={vc.name}>
                                {vc.name}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      );
                    } else if (index === 1) {
                      return <Typography>{vcList.length > 0 && selectedVC && vcList.find(item => item.name === selectedVC).description ? vcList.find(item => item.name === selectedVC).description : 'No description available'}</Typography>;
                    }
                  })()}
                  <Box sx={{ mb: 2 }}>
                    <div>
                      {activeStep === steps.length - 1 ? (
                        <>
                          {vcList.length > 0 && selectedVC && vcList.find(item => item.name === selectedVC).types.map((button) => (
                            <Button
                              key={button.type}
                              variant="contained"
                              onClick={() => handleStartIssuance(button.type)}
                              sx={{ mt: 1, mr: 1 }}
                            >
                              {button.type}
                            </Button>
                          ))}
                        </>
                      ) : (
                        <Button variant="contained" onClick={handleNext} sx={{ mt: 1, mr: 1 }} disabled={!selectedVC}>
                          Continue
                        </Button>
                      )}
                      <Button disabled={activeStep === 0} onClick={handleBack} sx={{ mt: 1, mr: 1 }}>
                        Back
                      </Button>
                    </div>
                  </Box>
                </StepContent>
              </Step>
            ))}
          </Stepper>
        </Box>
      </Box>
    ) : (
      <Box sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 2,
        minHeight: '100vh',
        textAlign: 'center',
      }}>
        <CircularProgress />
      </Box>
    )
  );
};

export default Issuer;