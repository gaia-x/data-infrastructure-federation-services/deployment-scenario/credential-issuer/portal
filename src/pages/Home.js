// import Box from '@mui/material/Box';
// import icpApi from 'services/icp-api';
// import Step from '@mui/material/Step';
// import { toast } from 'react-toastify';
// import Button from '@mui/material/Button';
// import Select from '@mui/material/Select';
// import Stepper from '@mui/material/Stepper';
// import { getOidcVersions } from 'utils/oidc';
// import MenuItem from '@mui/material/MenuItem';
// import StepLabel from '@mui/material/StepLabel';
// import Typography from '@mui/material/Typography';
// import InputLabel from '@mui/material/InputLabel';
// import StepContent from '@mui/material/StepContent';
// import FormControl from '@mui/material/FormControl';
// import { useLocation, useNavigate } from 'react-router-dom';
// import CircularProgress from '@mui/material/CircularProgress';

import React from 'react';
import { router } from "router";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const Home = () => {

  return (
    <Box>
      <Button variant="contained" onClick={() => router.navigate('/issuer')}>
        Navigate to Issuer
      </Button>
      <Button variant="contained" onClick={() => router.navigate('/verifier')} sx={{ ml: 2 }}>
        Navigate to Issuer
      </Button>
    </Box>
  );
};

export default Home;