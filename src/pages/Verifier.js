import { router } from "router";
import Box from '@mui/material/Box';
import icpApi from 'services/icp-api';
import { toast } from 'react-toastify';
import Button from '@mui/material/Button';
import Select from '@mui/material/Select';
import { getOidcVersions } from 'utils/oidc';
import MenuItem from '@mui/material/MenuItem';
import { useLocation } from 'react-router-dom';
import InputLabel from '@mui/material/InputLabel';
import Typography from '@mui/material/Typography';
import Presentation from 'components/Presentation';
import React, { useState, useEffect } from 'react';
import FormControl from '@mui/material/FormControl';
import CircularProgress from '@mui/material/CircularProgress';


const Verifier = () => {

  const [allPresentationDefinitions, setAllPresentationDefinitions] = useState([]);
  const [selectedPresentationDefinitions, setSelectedPresentationDefinitions] = useState('');

  const [vpToken, setVpToken] = useState('');

  const useQuery = () => {
    return new URLSearchParams(useLocation().search);
  };
  const query = useQuery();
  const state = query.get('state');
  const openidUri = query.get('openidUri');

  const handleChange = (event) => {
    setSelectedPresentationDefinitions(event.target.value);
  };

  const handleChildResponse = (response) => {
    if (response.format && response.format.length !== 0) {
      setVpToken(response);
    }
    else {
      toast.error("There was a problem with your request. Please try again.");
    }
  };

  const handleNewPresentation = async () => {
    try {
      const { oid4vpVersion, oid4vpPrefix } = getOidcVersions();
      const response = await icpApi.newPresentation({
        presentation_options: { oid4vp_version: oid4vpVersion, oidc_prefix: oid4vpPrefix },
        presentation_definition: selectedPresentationDefinitions
      });
      
      if (response.status === 200 && response.data !== '') {
        router.navigate(`?openidUri=${encodeURIComponent(response.data.openidUri)}&state=${encodeURIComponent(response.data.state)}`);
      }
    } catch (error) {
      toast.error(`An error occurred: ${error.response}`);
    }
  }

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await icpApi.getPresentationDefinitions();
        if (response.data && response.data !== '') {
          setAllPresentationDefinitions(response.data);
        }
        else {
          toast.error('Error to get Presentation Definitions, please try again.');
        }
      }
      catch (error) {
        toast.error(error.message);
      }
    }
    fetchData();
  }, []);

  return (
    (() => {
      if (vpToken && vpToken.format && vpToken.format.length !== 0) {
        return (<Box sx={{
          ml: '50px',
          padding: 2,
          alignItems: 'center',
          flexDirection: 'column',
          justifyContent: 'center'
        }}>
          {vpToken.format === 'ldp_vp' && (
            <Typography variant="h5" component="h3" sx={{ marginBottom: 2 }}>
              <pre>{JSON.stringify(vpToken.vp, null, 2)}</pre>
            </Typography>
          )}

          {vpToken.format === 'vc+sd-jwt' && (
            <>
              <Typography variant="h5" component="h3" sx={{ marginTop: 2, color: '#0000a0' }}>
                Claims:
              </Typography>
              <Box sx={{ width: '90%', bgcolor: 'background.paper' }}>
                <pre>
                  {JSON.stringify(vpToken.claims, null, 2)}
                </pre>
              </Box>

              <Typography variant="h5" component="h3" sx={{ marginTop: 2, color: '#0000a0' }}>
                Decoded SD-JWT:
              </Typography>
              <Box sx={{ width: '90%', overflow: 'auto', bgcolor: 'background.paper' }}>
                <pre>
                  {JSON.stringify(vpToken.decodedSdJwt, null, 2)}
                </pre>
              </Box>

              <Typography variant="h5" component="h3" sx={{ marginTop: 2, color: '#0000a0' }}>
                Raw SD-JWT:
              </Typography>
              <Box component="pre" sx={{ width: '90%', overflow: 'auto', bgcolor: 'background.paper' }}>
                <code id="openid-uri">
                  {vpToken.rawJWT}
                </code>
              </Box>
            </>
          )}

        </Box>
        )
      }
      else if (openidUri) {
        return (<Presentation openidUriPresentation={openidUri} state={state} handleChildResponse={handleChildResponse} />)
      }
      else if (allPresentationDefinitions) {
        return (
          <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            minHeight: '93vh',
            background: 'linear-gradient(270deg,#000f8e 25%,#a931f6 129.76%)',
            padding: '20px',
          }}>
            <Box sx={{ maxWidth: 400, width: '100%', backgroundColor: '#fff', borderRadius: '5px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)', margin: '20px', padding: '20px' }}>
              <Typography sx={{ marginBottom: 2 }}>
                Select a Presentation Definition to make
              </Typography>
              <FormControl fullWidth>
                <InputLabel id="vc-select-label">Presentation Definitions</InputLabel>
                <Select
                  labelId="vc-select-label"
                  id="vc-select"
                  value={selectedPresentationDefinitions}
                  onChange={handleChange}
                  label="Presentation Definitions"
                >
                  {allPresentationDefinitions.map((presentationDefinition) => (
                    <MenuItem key={presentationDefinition} value={presentationDefinition}>
                      {presentationDefinition}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <Button variant="contained" onClick={handleNewPresentation} sx={{ mt: 1, mr: 1 }} disabled={!selectedPresentationDefinitions}>
                Continue
              </Button>
            </Box>
          </Box>
        )
      }
      else {
        return (
          <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 2,
            minHeight: '100vh',
            textAlign: 'center',
          }}>
            <CircularProgress />
          </Box>
        )
      }
    })()
  );
};

export default Verifier;