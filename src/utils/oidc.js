export function getOidcVersions() {
    const oid4vciVersion = localStorage.getItem('oid4vciVersion');
    const oid4vpVersion = localStorage.getItem('oid4vpVersion');
    const issuanceMode = localStorage.getItem('issuanceMode');
    const oid4vciPrefix = localStorage.getItem('oid4vciPrefix');
    const oid4vpPrefix = localStorage.getItem('oid4vpPrefix');
    
    if (!oid4vciVersion || !oid4vpVersion || !issuanceMode) {
        throw new Error("OIDC options unknown, please select a version from the 'OIDC Options' tab at the top of the page.");
    }
    
    return { oid4vciVersion, oid4vpVersion, issuanceMode, oid4vciPrefix, oid4vpPrefix };
}