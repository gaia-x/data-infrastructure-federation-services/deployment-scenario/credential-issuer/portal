import axios from 'axios';
import { getOidcVersions } from 'utils/oidc';

const getIcpApiUrl = () => {
    if (process.env.NODE_ENV !== 'production') {
        if (!process.env.REACT_APP_ICP_URL) throw new Error('Must set env variable $REACT_APP_ICP_URL');
        return process.env.REACT_APP_ICP_URL;
    }
    
    return "__REACT_APP_ICP_URL__";
};

const api = {
    getPresentationDefinitions: async () => {
        return await axios.get(`${getIcpApiUrl()}/presentation-definitions`)
            .then((res) => res)
            .catch((err) => err)
    },

    newPresentation: async (data) => {
        return await axios.post(`${getIcpApiUrl()}/presentation`, data)
            .then((res) => res)
            .catch((err) => err)
    },

    getVcToIssue: async () => {
        return await axios.get(`${getIcpApiUrl()}/issuance`)
            .then((res) => res)
            .catch((err) => err)
    },

    startIssuance: async (vcName, typeOfIssuance) => {
        const { oid4vciVersion, oid4vpVersion, issuanceMode } = getOidcVersions();
        const body = {
            action: "startIssuance",
            issuance_options: {
                oid4vci_version: oid4vciVersion,
                mode: issuanceMode
            },
            presentation_options: {
                oid4vp_version: oid4vpVersion
            },
            type_of_issuance: typeOfIssuance
        };
        return axios.post(`${getIcpApiUrl()}/issuance/${vcName}`, body)
            .then((res) => res)
            .catch((err) => err);
    },
    
    continueIssuance: async (vcName, data) => {
        const { oid4vciVersion, oid4vpVersion, issuanceMode, oid4vciPrefix } = getOidcVersions();
        const body = {
            action: "continueIssuance",
            issuance_options: {
                oid4vci_version: oid4vciVersion,
                mode: issuanceMode,
                oidc_prefix: oid4vciPrefix
            },
            presentation_options: {
                oid4vp_version: oid4vpVersion
            },
            data: data
        };
        return axios.post(`${getIcpApiUrl()}/issuance/${vcName}`, body)
            .then((res) => res)
            .catch((err) => err);
    },

    authorize: async (state, username) => {
        return axios.post(`${getIcpApiUrl()}/presentation/authorize`, { ...state, username })
            .then((res) => res)
            .catch((err) => err);
    },
    
    isVerified: async (state) => {
        return axios.get(`${getIcpApiUrl()}/presentation/${state}`)
            .then((res) => res)
            .catch((err) => err);
    }
}

export default api;