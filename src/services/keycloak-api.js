import axios from 'axios';

const getKeycloakApiUrl = () => {
    if (process.env.NODE_ENV !== 'production') {
        if (!process.env.REACT_APP_KEYCLOAK_URL) throw new Error('Must set env variable $REACT_APP_KEYCLOAK_URL');
        return process.env.REACT_APP_KEYCLOAK_URL;
    }
    
    return "__REACT_APP_KEYCLOAK_URL__";
};

const keycloak = {
    // Get the token keycloak with the code from the URL
    getToken: async (vcName, code) => {
        try {
            const body = {
                grant_type: 'authorization_code',
                client_id: 'icp-portail',
                redirect_uri: `${window.location.protocol}//${window.location.host}/issuer/${encodeURIComponent(vcName)}`,
                code: code
            };
            const headers = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
            const response = await axios.post(getKeycloakApiUrl(), body, headers);
            if (response.status === 200 && response.data !== '') {
                return response.data.access_token;
            }
        }
        catch (error) {
            console.log(error);
        }
    }
}

export default keycloak; 