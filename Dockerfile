FROM node:alpine AS build
RUN addgroup -S appgroup && adduser -S appuser -G appgroup
WORKDIR /usr/src/app
COPY package*.json ./
COPY jsconfig.json ./
RUN npm ci --silent
COPY ./src ./src
COPY ./public ./public
RUN npm run build

FROM nginx:alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/
WORKDIR /usr/src/app
EXPOSE 3000

COPY ./startup.sh ./
RUN chmod +x ./startup.sh
CMD [ "/bin/sh", "-c", "/usr/src/app/startup.sh" ]